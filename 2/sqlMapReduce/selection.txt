SELECT unique(fieldA) FROM "input.csv" WHERE fieldA == 198

processes tuples

map(record) generates tuples (key, value)
reduce(key, list<values>) generate tuples (key, values)

function map(line)
	array = split line on ","
of array["fieldA"] > 198:
	emit(fieldA, fieldA)

function reduce(key, list of values):
emit(key)

